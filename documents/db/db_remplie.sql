CREATE DATABASE  IF NOT EXISTS `platz` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `platz`;
-- MySQL dump 10.13  Distrib 5.6.34, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: platz
-- ------------------------------------------------------
-- Server version	5.6.34-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(45) NOT NULL,
  `icone` varchar(100) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'Illustrator','icon-ai.svg',NULL,NULL),(2,'Photoshop','icon-psd.svg',NULL,NULL),(3,'Wordpress','icon-themes.svg',NULL,NULL),(4,'Free fonts','icon-font.svg',NULL,NULL),(5,'Photo','icon-photo.svg',NULL,NULL),(6,'All','icon-premium.svg',NULL,NULL);
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `commentaires`
--

DROP TABLE IF EXISTS `commentaires`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `commentaires` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pseudo` varchar(45) NOT NULL,
  `texte` text NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `ressources` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_commentaires_ressources1_idx` (`ressources`),
  CONSTRAINT `fk_commentaires_ressources1` FOREIGN KEY (`ressources`) REFERENCES `ressources` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `commentaires`
--

LOCK TABLES `commentaires` WRITE;
/*!40000 ALTER TABLE `commentaires` DISABLE KEYS */;
INSERT INTO `commentaires` VALUES (1,'Bouchra','Lorem',NULL,NULL,4),(2,'Sarah','Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.',NULL,NULL,4),(3,'Sonia','Lorem ipsum dolor sit amet, consetetur ',NULL,NULL,5),(9,'Hajar','Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.','2020-06-14 16:10:16','2020-06-14 16:10:16',4),(10,'Hajar','Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.','2020-06-14 16:10:32','2020-06-14 16:10:32',9),(11,'Lorem','Lorem ipsum dolor sit amet, consetetur','2020-06-14 16:12:43','2020-06-14 16:12:43',8),(12,'Sarah','Lorem ipsum dolor sit amet, consetetur sadipscing elit','2020-06-14 19:45:32','2020-06-14 19:45:32',4),(13,'Hajar','Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.','2020-06-15 07:30:43','2020-06-15 07:30:43',6);
/*!40000 ALTER TABLE `commentaires` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ressources`
--

DROP TABLE IF EXISTS `ressources`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ressources` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(45) NOT NULL,
  `image` varchar(100) NOT NULL,
  `description` text,
  `auteur` varchar(45) DEFAULT NULL,
  `taille` varchar(45) DEFAULT NULL,
  `download` varchar(100) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `categories` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_ressources_categories_idx` (`categories`),
  CONSTRAINT `fk_ressources_categories` FOREIGN KEY (`categories`) REFERENCES `categories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ressources`
--

LOCK TABLES `ressources` WRITE;
/*!40000 ALTER TABLE `ressources` DISABLE KEYS */;
INSERT INTO `ressources` VALUES (4,'Lorem ipsum dolor sit amet','psd-4.jpg','Symphony is a responsive one page website template designed with sketches and coded with html5 and php. Freebie released by Lacoste Xavier.','Onur','23 Mo','Document PDF.pdf','2020-09-25 00:00:00',NULL,3),(5,'Font','font-1.jpg','Bavro is a minimal free font best suitable for posters and headlines. Designed and released by Marcelo Melo.','Alicia','24 Mo','Document PDF.pdf','2020-05-10 10:00:00',NULL,4),(6,'PSD Goodies','psd-1.jpg','A set of high resolution iPhone 6 and Nexus 5 mockups created with smart objects. Free PSD released by Ghani Pradita.','Nawal','25 Mo','Document PDF.pdf','2020-05-10 10:00:00',NULL,2),(7,'PSD Goodies','psd-2.jpg','A set of 4 free photorealistic Nexus 5 mockups providing smart objects. Free PSD released by Craftwork.','Maxim','23 Mo','Document PDF.pdf','2020-05-10 10:00:00',NULL,2),(8,'Illustrator Freebies','ai-1.jpg','A set of 6 outline beer icons created with Adobe Illustrator. Free Ai designed and released by Justas Galaburda.','Natacha','23 Mo','Document PDF.pdf','2020-05-10 10:00:00',NULL,1),(9,'Html theme','theme-2.jpg','Symphony is a responsive one page website template designed with sketches and coded with html5 and php. Freebie released by Lacoste Xavier.','Onuur','23 Mo','Document PDF.pdf','2020-05-10 10:00:00',NULL,3),(10,'PSD Goodies','psd-3.jpg','Symphony is a responsive one page website template designed with sketches and coded with html5 and php. Freebie released by Lacoste Xavier.','Stéphanie','23 Mo','Document PDF.pdf','2020-05-10 10:00:00',NULL,2),(11,'Lorem ipsum ','font-2.jpg','A set of high resolution iPhone 6 and Nexus 5 mockups created with smart objects. Free PSD released by Ghani Pradita.','Sarah','45 Mo','Document PDF.pdf','2020-05-10 10:00:00',NULL,4),(24,'Wordpress theme','font-3.jpg','A set of 4 free photorealistic Nexus 5 mockups providing smart objects. Free PSD released by Craftwork.','Chaimae','30 Mo','Document PDF.pdf','2020-05-10 10:00:00',NULL,3),(25,'Illustrator icons','ai-2.jpg','A set of 6 outline beer icons created with Adobe Illustrator. Free Ai designed and released by Justas Galaburda.','Luca','27 Mo','Document PDF.pdf','2020-05-10 10:00:00',NULL,1),(26,'PSD icons','icons-1.jpg','Symphony is a responsive one page website template designed with sketches and coded with html5 and php. Freebie released by Lacoste Xavier.','Zak','30 Mo','Document PDF.pdf','2020-05-10 10:00:00',NULL,2),(27,'UI design','ui-1.jpg','Acess to our largest database of the web about the ui and look into a ton of professionnal tools','Onuur','28 Mo','Document PDF.pdf','2020-05-10 10:00:00',NULL,5),(28,'Free font','font-5.jpg','A set of 6 outline beer icons created with Adobe Illustrator. Free Ai designed and released by Justas Galaburda.','Jhon','50 Mo','Document PDF.pdf','2020-05-10 10:00:00',NULL,4),(29,'Html theme','theme-2.jpg','Symphony is a responsive one page website template designed with sketches and coded with html5 and php. Freebie released by Lacoste Xavier.','Onuur','40 Mo','Document PDF.pdf','2020-05-10 10:00:00',NULL,3),(30,'PSD mockup','psd-5.jpg','A very detailed Macbook Air mockup created with Photoshop and providing smart objects. Free PSD released by Barin Cristian.','Philippe','35 Mo','Document PDF.pdf','2020-05-10 10:00:00',NULL,2),(31,'PSD icons','icons-3.jpg','A set of 16 hand gestures icons you may find useful for your projects. Free PSD released by Rovane Durso.','Onuur','29 Mo','Document PDF.pdf','2020-05-10 10:00:00',NULL,2),(32,'Free font','font-4.jpg','Julep is an elegant and modern free font released in vector formats (Ai, EPS and PDF). Designed and released by Jeremy Ross.','Sarah','25 Mo','Document PDF.pdf','2020-05-10 10:00:00',NULL,4),(33,'HTML theme','theme-3.jpg','Gorgo is a free HTML portfolio template for freelancers, photographers, agencies, designers and other creative fields. Designed and released by Aristothemes.','Philippe','60 Mo','Document PDF.pdf','2020-05-10 10:00:00',NULL,3),(34,'Free font','font-6.jpg','REEF is a rounded font free for commercial and personal use. It\'s strength lies in simplicity and construction.','Onuur','23 Mo','Document PDF.pdf','2020-05-10 10:00:00',NULL,4),(35,'HTML theme','theme-4.jpg','ActiveBox is a free responsive HTML template featured by clean and minimalistic design. Designed and coded by Kamal Chaneman.','Jhon','43 Mo','Document PDF.pdf','2020-05-10 10:00:00',NULL,3),(41,'Free font','font-5.jpg','A set of 6 outline beer icons created with Adobe Illustrator. Free Ai designed and released by Justas Galaburda.','Onuur','40 Mo','Document PDF.pdf','2020-05-10 10:00:00',NULL,4),(42,'Mockup','ui-1.jpg','ActiveBox is a free responsive HTML template featured by clean and minimalistic design. Designed and coded by Kamal Chaneman.','Jamal','50 Mo',NULL,'2020-05-10 10:00:00',NULL,5),(43,'Illustrator icons','ai-2.jpg','A set of 6 outline beer icons created with Adobe Illustrator. Free Ai designed and released by Justas Galaburda.','Onuur','40 Mo',NULL,'2020-05-10 10:00:00',NULL,1),(44,'PSD Goodies','psd-2.jpg','Julep is an elegant and modern free font released in vector formats (Ai, EPS and PDF). Designed and released by Jeremy Ross.','Onuur','27 Mo',NULL,'2020-05-10 10:00:00',NULL,2),(45,'Ipad Mockup','mockup-1.jpg','REEF is a rounded font free for commercial and personal use. It\'s strength lies in simplicity and construction.','Onuur','25 Mo',NULL,'2020-05-10 10:00:00',NULL,5),(46,'Illustrator Freebies','ai-1.jpg','A set of 6 outline beer icons created with Adobe Illustrator. Free Ai designed and released by Justas Galaburda.','Onuur','50 Mo',NULL,'2020-05-10 10:00:00',NULL,1),(47,'Wordpress theme','font-3.jpg','A set of 4 free photorealistic Nexus 5 mockups providing smart objects. Free PSD released by Craftwork.','Jean','50 Mo',NULL,'2020-05-10 10:00:00',NULL,3),(48,'Illustrator Freebies','icons-3.jpg','A set of 6 outline beer icons created with Adobe Illustrator. Free Ai designed and released by Justas Galaburda.','Onuur','43 Mo',NULL,'2020-05-10 10:00:00',NULL,1),(49,'Free font','icons-2.jpg','A set of 6 outline beer icons created with Adobe Illustrator. Free Ai designed and released by Justas Galaburda.','Onuur','60 Mo',NULL,'2020-05-10 10:00:00',NULL,4),(50,'Photo','mockup-2.jpg','Acess to our largest database of the web about the ui and look into a ton of professionnal tools','Onuur','30 Mo',NULL,'2020-05-10 10:00:00',NULL,5),(51,'PSD Goodies','psd-2.jpg','A set of 4 free photorealistic Nexus 5 mockups providing smart objects. Free PSD released by Craftwork.','Onuur','23 Mo',NULL,'2020-05-10 10:00:00',NULL,2),(52,'Illustrator icons','ai-2.jpg','A set of 6 outline beer icons created with Adobe Illustrator. Free Ai designed and released by Justas Galaburda.','Hajar','40 Mo',NULL,'2020-05-10 10:00:00',NULL,1),(53,'Html theme','theme-2.jpg','Symphony is a responsive one page website template designed with sketches and coded with html5 and php. Freebie released by Lacoste Xavier.','Lorem','23 Mo',NULL,'2020-05-10 10:00:00',NULL,3),(54,'Wordpress theme','font-3.jpg','A set of 4 free photorealistic Nexus 5 mockups providing smart objects. Free PSD released by Craftwork.','Alain','50 Mo',NULL,'2020-05-10 10:00:00',NULL,3),(55,'Photo','mockup-2.jpg','Acess to our largest database of the web about the ui and look into a ton of professionnal tools','Jamal','30 Mo',NULL,'2020-05-10 10:00:00',NULL,5),(56,'PSD Goodies','psd-2.jpg','Julep is an elegant and modern free font released in vector formats (Ai, EPS and PDF). Designed and released by Jeremy Ross.','Onuur','27 Mo',NULL,'2020-05-10 10:00:00',NULL,2),(57,'Phasellus','icons-2.jpg','A set of 6 outline beer icons created with Adobe Illustrator. Free Ai designed and released by Justas Galaburda.','Jean','50 Mo',NULL,'2020-05-10 10:00:00',NULL,3),(58,'Donec','psd-1.jpg','Julep is an elegant and modern free font released in vector formats (Ai, EPS and PDF). Designed and released by Jeremy Ross.','Hajar','23 Mo',NULL,'2020-05-10 10:00:00',NULL,2),(59,'Nam at','psd-3.jpg','A set of 6 outline beer icons created with Adobe Illustrator. Free Ai designed and released by Justas Galaburda.','Stéphanie','43 Mo',NULL,'2020-05-10 10:00:00',NULL,2),(60,'Aenean leo','theme-4.jpg','ActiveBox is a free responsive HTML template featured by clean and minimalistic design. Designed and coded by Kamal Chaneman.','David','36',NULL,'2020-05-10 10:00:00',NULL,3);
/*!40000 ALTER TABLE `ressources` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-06-15 12:06:59
