let getters = {
    getRessources (state) {
      return state.ressources;
      return this.ressources.filter((ressource) => {
        return ressource.nom.match(this.search);
      })
    },
    getRessourceById (state) {
      return function (id) {
        return state.ressources.find(ressource => ressource.id == id)
      }
      return this.ressources.filter((ressource) => {
        return ressource.nom.match(this.search);
      })
    },
    getCategories (state) {
      return state.categories;
    },
    getRessourcesByCategorieId (state) {
      return function (id) {
        return state.ressources.filter(ressources => ressources.categorie.id == id)
      }
      return this.ressources.filter((ressource) => {
        return ressource.nom.match(this.search);
      })
    },
    getCommentaires (state) {
      return state.commentaires;
    },
    getCommentairesByRessourceId (state) {
      return function (id) {
        return state.commentaires.filter(commentaires => commentaires.ressources == id)
      }
    },
  };

  export default getters;
