let  mutations = {

    // LES RESSOURCES
    SET_RESSOURCES (state, data) {
      state.ressources = data.map(el => {
        //console.log(el);
        el['isActive'] = false
        return el;
      });
    },

    // LES CATEGORIES
    SET_CATEGORIES (state, data) {
      state.categories = data;
    },

    // LES COMMENTAIRES
    SET_COMMENTAIRES (state, data) {
      state.commentaires = data;
    },
    CREATE_COMMENTAIRE (state, commentaire) {
      state.commentaires.unshift(commentaire)
    }
  };

  export default mutations;
