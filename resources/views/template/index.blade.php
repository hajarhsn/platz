<!DOCTYPE HTML>
<html lang="en">

<head>
  @include('template.partials._head')
</head>

  <body>
    <div id="app">
      <!-- HEADER -->
      <header-component></header-component>

      <!-- PORTFOLIO -->
    	<div id="wrapper-container">
        <div class="container object">
          <div id="main-container-image">
            <router-view></router-view>
          </div>
        </div>
      </div>
      <!-- FOOTER -->
      <footer-component></footer-component>
    </div>

  <!-- SCRIPT -->
   @include('template.partials._scripts')
  </body>
</html>
