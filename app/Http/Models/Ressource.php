<?php
  namespace App\Http\Models;
  use Illuminate\Database\Eloquent\Model;

  class Ressource extends Model {
    /**
     * The table associated with the model.
     * @var string
     */
    protected $table = 'ressources';
  

    /*
      |--------------------------------------------------------------------------
      | RELATIONS
      |--------------------------------------------------------------------------
    */

    public function categorie(){
        return $this->belongsTo('App\Http\Models\Categorie', 'categories');
    }

    public function commentaire() {
      return $this->hasMany('App\Http\Models\Commentaire', 'ressource');
    }
  }
