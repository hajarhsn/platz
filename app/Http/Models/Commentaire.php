<?php
  namespace App\Http\Models;
  use Illuminate\Database\Eloquent\Model;

  class Commentaire extends Model {
    /**
     * The table associated with the model.
     * @var string
     */

    protected $table = 'commentaires';
    protected $fillable = ['pseudo', 'texte', 'ressources'];

    /*
      |--------------------------------------------------------------------------
      | RELATIONS
      |--------------------------------------------------------------------------
    */

    public function ressource(){
        return $this->belongsTo('App\Http\Models\Ressource', 'ressources');
    }
  }
